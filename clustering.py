# imports

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sn

from sklearn.cluster import KMeans


# load data
large_matrix = np.loadtxt("mymatrix.txt", skiprows=1, dtype=int)


X = large_matrix[:, 0:2] # data points
W = large_matrix[:, 2] # weights
P = large_matrix[:, -1] # old partitions

n = X.shape[0]
m = 1500


def plot_matrix(X, P):
    data = pd.DataFrame({
        'x' : X[:, 0],
        'y' : X[:, 1],
        'p' : P
    })
    sn.scatterplot(data=data, x='x', y='y', hue='p', legend=False, palette='Accent')
    plt.show()

    
def elastic_gradient_whatever(X, W, k, decreasing=False):
    """solution semble être instable

    Args:
        X (_type_): _description_
        W (_type_): _description_
        k (_type_): _description_

    Returns:
        _type_: _description_
    """
    # initialize centers
    X = X - X.mean()
    total_weight = W.sum()
    clustering = KMeans(n_clusters=k)
    clustering.fit(X, sample_weight=W)
    new_centers = clustering.cluster_centers_
    P = clustering.labels_

    Pw = np.array([W[P == p].sum() for p in np.arange(k)], dtype=float)
    centers = None
    iter = -1
    qualite = np.Inf
    max_legal = 0.85
    while  (Pw.min()/Pw.max() < max_legal) and iter < m:
        centers = new_centers.copy()
        iter += 1
        # update partitions

        distances = np.linalg.norm(X[:, None, :] - centers[None, :, :], axis=-1)
        # distances *= Pw
        P = np.argmin(distances, axis = 1)
        s = set(P)
        if len(s) != k:
            print(f"failed, iter = {iter}")
        

        ### update centers

        new_centers = np.zeros((k, 2))

        # array containing weight of every partitions
        Pw = np.array([W[P == p].sum() for p in np.arange(k)], dtype=float)
        dispersion = Pw.std()

        # distance matrix between points
        V = np.linalg.norm(centers[:, None, :] - centers[None, :, :], axis=-1)

        # distance matrix between points weights
        dW = np.subtract.outer(Pw, Pw)

        # iteration here because I can't figure out how to 
        # do 3d matrix of vectors substractions
        for index, center in enumerate(centers):
            all_distances = V[index, :]**2
            p = dW[:, index]#/all_distances
            p = p/total_weight

            v = centers - center

            u = np.nan_to_num(v / all_distances.reshape((k, 1)))


            u = u * p.reshape((k, 1))
            u = u.sum(axis = 0)

            u *= V[index, :].sum()

            if decreasing:
                u *= (dispersion*k/total_weight)
                # u *= np.sqrt(dispersion*k/total_weight)
            

            new_centers[index] = center + u 

        if dispersion < qualite:
            qualite = dispersion
            real_P = P
            print(f"à l'iteration {iter}, une qualité de {qualite} et un coef de {qualite*k/total_weight}", end='\r')

    return real_P




k = 75 # number of cluster

P = elastic_gradient_whatever(X, W, 75)

Pw = np.array([W[P == p].sum() for p in np.arange(k)])
print(f"le minimum est : {Pw.min()}, le maximum est {Pw.max()}")
print(f"ce qui fait une différence de {- Pw.min() + Pw.max()}")
print(f"les bornes sont {round(Pw.mean()*0.8)}, {round(Pw.mean()*1.2)}")


plot_matrix(X, P)

