Constituency Maker
==================


## L'objectif

Il s'agit de découper un territoire en circonscriptions connexes d'égales populations. À ma grande surprise il s'agit d'un problème de clustering qui n'a pas de solution très connue et efficace.

## Les fichiers

Il y a un fichier .R qui reprend les fonctions de l'INSEE pour utiliser les données carroyées. Et un fichier .py avec l'algorithme retenu actuellement.

## Les tentatives

Beaucoup de post utilisent une technique consistant à :* appliquer les K-means (ou autre algo de clustering)* Chaque cluster va tour à tour capturer ou se délester d'un point en fonction de sa population.
Cette solution génère souvent des découpages non connexes, même en prenant beaucoup de précautions.

## Pour l'instant

L'idée sur laquelle je me suis arrêté consiste à simuler une sorte de système physique. C'est très lent, mais assez performant pour l'usage que j'en ai.
Afin de garantir des circonscriptions connexes (même convexes en l'occurrence, même si ce n'est pas l'objectif), j'ai conservé le principe de définir les groupes par des centroïdes.
La fonction d'itération est inspirée d'un système de mécanique. Les centroïdes sont attirés entre eux par une "force" qui :* décroit au carré de la distance entre deux points.* dépend de l'écart de masse entre les deux points (elle peut être négative).
Une configuration où toutes les circonscriptions ont une même population est invariante par la fonction, toutes les forces étant nulles.
Plus formellement, on dispose des $d$ vecteurs des centroïdes $`\{c_k, k < d\}`$, tous associés aux poids $`\{w_k, k < d\}`$.
À chaque itération, la position du centroïde $c_k$ varie du vecteur $a_k$ défini comme tel :

```math
a_k = \sum_{i < d}\frac{w_i - w_k}{w_T}\frac{d_T}{\lVert c_i - c_k \rVert ^2}(c_i - c_k)
```

avec $w_t$ la population totale, et $`d_T = \sum_{i < d} \lVert c_i - c_k \rVert ^2`$ la somme de toutes les distances des points au point $k$.
Une interprétation claire de cette formule est en cours d'élaboration.

## La convergence

Cet algorithme est, il faut le dire, très *grossier*. Il n'a rien de rigoureux dans son élaboration. Il n'a donc aucune raison de converger vers quoi que ce soit. Il semble cependant que les centroïdes oscillent autour d'un minimum local.
On peut ajouter un terme qui va diminuer la taille des pas avec la dispersion de la population des circonscriptions. Cependant cela semble nuire aux performances.

![image](figures/comparaison.png)

## Futur


Incertain.


## Autre solution


En travaillant sur les bureaux de vote, il semble qu'un algo s'inspirant de la Classification Ascendante Hiérarchique (CAH) puisse fonctionner. Des trucs dans le notebook **regroupement_hierarchique.ipynb** À voir.